import React from 'react';
import {
  filterByAttemptCount,
  filterReadingExercises,
  filterOwnTextAttempts,
  filterFirstReadingAttempts,
  filterByExerciseName,
} from './util/groupTable';
import {
  lowerBoundOutlierFilter,
  upperBoundOutlierFilter,
  filterStandardDeviation,
} from '../../../containers/Statistics/util/statistics';
import GroupRegressionChart from './GroupRegressionChart';

export default function GroupChart(props) {
  const {
    data,
    isTeacher,
    exercise,
    minimumAttemptCount,
    filterOwnTexts = false,
    filterFirstReadingAttempt = false,
    order,
    title,
    yLabel,
    yFields,
    timeFilter,
    translate,
  } = props;

  const filteredGroupData = Object.assign(
    {},
    ...Object.keys(data).map((userId) => {
      const boundFiltered = data[userId]
        .filter((attempt) => upperBoundOutlierFilter(attempt))
        .filter((attempt) => lowerBoundOutlierFilter(attempt));
      const standardDeviationFiltered = [
        ...filterStandardDeviation('readingExercises', boundFiltered),
        ...filterStandardDeviation('schulteTables', boundFiltered),
        ...filterStandardDeviation('concentration', boundFiltered),
        ...filterStandardDeviation('visualVocabulary', boundFiltered),
      ];
      const filteredData = standardDeviationFiltered.filter(timeFilter);
      return {
        [userId]: filteredData,
      };
    }),
  );

  let readingExerciseData = filterReadingExercises(filteredGroupData);
  readingExerciseData = filterOwnTextAttempts(readingExerciseData, filterOwnTexts);
  readingExerciseData = filterFirstReadingAttempts(readingExerciseData, filterFirstReadingAttempt);
  let schulteTablesExerciseData = filterByExerciseName(filteredGroupData, 'schulteTables');
  let concentrationExerciseData = filterByExerciseName(filteredGroupData, 'concentration');
  let visualVocabularyExerciseData = filterByExerciseName(filteredGroupData, 'visualVocabulary');

  if (isTeacher) {
    readingExerciseData = filterByAttemptCount(readingExerciseData, minimumAttemptCount);
    schulteTablesExerciseData = filterByAttemptCount(schulteTablesExerciseData, minimumAttemptCount);
    concentrationExerciseData = filterByAttemptCount(concentrationExerciseData, minimumAttemptCount);
    visualVocabularyExerciseData = filterByAttemptCount(visualVocabularyExerciseData, minimumAttemptCount);
  }

  let chartData = Object.keys(readingExerciseData).map((userId, index) =>
    readingExerciseData[userId].map((userData) => ({
      ...userData,
      userId,
      color: 'hsl(' + (index / Object.keys(readingExerciseData).length) * 360 + ',50%,50%)',
    })),
  );
  if (exercise !== 'readingExercises') {
    chartData = chartData.map((userExerciseData) =>
      userExerciseData.filter((exerciseData) => exerciseData.exercise === exercise),
    );
  }

  return (
    <>
      <div style={{ textAlign: 'center', overflowX: 'auto' }}>
        <GroupRegressionChart
          width={1000}
          height={400}
          groupData={chartData}
          xField="index"
          yField={yFields[0]}
          count={+minimumAttemptCount}
          order={order}
          title={title}
          xLabel={translate('regression-chart.index')}
          yLabel={yLabel}
          translate={translate}
        />
      </div>
    </>
  );
}
