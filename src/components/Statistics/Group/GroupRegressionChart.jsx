import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { scaleLinear } from 'd3-scale';
import { axisBottom, axisLeft } from 'd3-axis';
import 'd3-transition';
import { select } from 'd3-selection';
import { interpolatePath } from 'd3-interpolate-path';
import { line, curveBasis } from 'd3-shape';

import './GroupRegressionChart.css';
import { getAverage, polynomial, calculateY } from '../../../shared/utility';

const margin = {
  top: 50,
  right: 30,
  bottom: 20,
  left: 50,
};

const DATA_POINT_RADIUS = 4.5;
const TRANSITION_DURATION = 2000;
const FONT_SIZE = 12;
const HORIZONTAL_SPACING = 0.05;
const VERTICAL_SPACING = 0.05;

export class GroupRegressionChart extends Component {
  state = {
    width: this.props.width - margin.left - margin.right,
    height: this.props.height - margin.top - margin.bottom,
    widthScale: scaleLinear().range([0, this.props.width - margin.left - margin.right]),
    heightScale: scaleLinear().range([0, this.props.height - margin.top - margin.bottom]),
  };

  static getDerivedStateFromProps(nextProps, prevState) {
    let { widthScale } = prevState;
    const { groupData } = nextProps;

    const width = nextProps.width - margin.left - margin.right;
    if (groupData.length > 0) {
      widthScale = scaleLinear().range([0, width]);
    }

    return {
      ...prevState,
      widthScale,
    };
  }

  componentDidMount() {
    const { width, height, widthScale, heightScale } = this.state;
    let { groupData } = this.props;
    const { title, xField, yField, translate, count, order } = this.props;

    groupData = groupData
      .map((userData) => userData.map((fields, index) => ({ index: index + 1, ...fields })))
      .filter((list) => list.length >= count)
      .map((list) => list.slice(0, count));

    const flatData = groupData.flatMap((userData) => userData);
    const xValues = flatData.map((d) => d[xField]);
    const yValues = flatData.map((d) => d[yField]);
    const minXValue = Math.min(...xValues);
    const maxXValue = Math.max(...xValues);
    const maxYValue = Math.max(...yValues);
    const horizontalSpacing = (maxXValue - minXValue) * HORIZONTAL_SPACING || 1;
    const verticalSpacing = maxYValue * VERTICAL_SPACING;
    this.xScale = widthScale.domain([minXValue - horizontalSpacing, maxXValue + horizontalSpacing]);
    this.yScale = heightScale.domain([maxYValue + verticalSpacing, -verticalSpacing]);

    const sizeArray = groupData.length > 0 ? [...Array(count)] : [];
    const xSeries = sizeArray.map((_, index) => this.xScale(index + 1));
    const ySeries = sizeArray.map((_, index) => {
      const values = groupData.map((userData) => userData[index][yField]);
      return this.yScale(getAverage(values));
    });
    const result = polynomial(xSeries, ySeries, order);
    const equation = calculateY(result.equation);

    const svg = select(this.svgRef);

    // Append title
    svg
      .append('text')
      .attr('class', 'title')
      .style('font-size', FONT_SIZE + 5)
      .style('font-weight', 'bold')
      .attr('transform', `translate(${margin.left - 10}, ${FONT_SIZE + 3})`)
      .text(title);

    // Append no data message
    svg
      .append('text')
      .attr('class', 'no-data')
      .attr('dy', '-.7em')
      .style('opacity', flatData.length === 0 ? 1 : 0)
      .style('text-anchor', 'middle')
      .style('user-select', 'none')
      .attr('x', margin.left + width / 2)
      .attr('y', margin.top + height / 2)
      .text(translate('regression-chart.no-data'));

    const chart = svg.append('g').attr('class', 'chart').attr('transform', `translate(${margin.left}, ${margin.top})`);

    // Append axis
    chart
      .append('g')
      .attr('class', 'x-axis')
      .attr('transform', `translate(0, ${height})`)
      .style('font-size', FONT_SIZE)
      .call(axisBottom(this.xScale).tickSizeInner(-height))
      .append('text')
      .attr('class', 'label')
      .attr('y', -2)
      .attr('x', width)
      .text(this.props.xLabel);
    chart
      .append('g')
      .attr('class', 'y-axis')
      .style('font-size', FONT_SIZE)
      .call(axisLeft(this.yScale).ticks(10).tickSizeInner(-width))
      .append('text')
      .attr('class', 'label')
      .attr('transform', 'rotate(-90)')
      .attr('y', FONT_SIZE)
      .text(this.props.yLabel);

    // Append tooltip
    if (select('#tooltip').empty()) {
      select('#root').append('div').attr('id', 'tooltip');
    }
    select('#tooltip').style('visibility', 'hidden');

    // Append datapoints
    chart
      .append('g')
      .selectAll('.data-point')
      .data(flatData, (d) => d.id)
      .enter()
      .append('circle')
      .attr('class', 'data-point')
      .attr('cx', (d) => this.xScale(d[xField]))
      .attr('cy', (d) => this.yScale(d[yField]))
      .attr('r', DATA_POINT_RADIUS)
      .style('fill', (d) => d.color)
      .style('stroke', (d) => d.color)
      .on('mouseover', (event, d) => {
        this.updateTooltip(event, d, yField);
      })
      .on('mouseout', () => {
        select('#tooltip').style('visibility', 'hidden');
      });

    // Append regression line
    const newLine = line()
      .curve(curveBasis)
      .x((d) => d)
      .y((d) => equation(d));

    chart.append('path').attr('class', 'regression-line').style('stroke', 'red').datum(xSeries).attr('d', newLine);
  }

  componentDidUpdate(prevProps) {
    const { width, height } = this.state;
    let { groupData } = this.props;
    const { title, xLabel, yLabel, xField, yField, count, order } = this.props;
    const { widthScale, heightScale } = this.state;
    const svg = select(this.svgRef);
    const chart = svg.select('g');

    if (prevProps.title !== title) {
      svg.select('.title').text(title);
    }
    if (prevProps.xField !== xField) {
      chart.select('.x-axis .label').transition().duration(TRANSITION_DURATION).text(xLabel);
    }
    if (prevProps.yField !== yField) {
      chart.select('.y-axis .label').transition().duration(TRANSITION_DURATION).text(yLabel);
    }
    if (select('#tooltip').empty()) {
      select('#root').append('div').attr('id', 'tooltip');
    }
    // Move data from render method to parent state
    if (prevProps.groupData !== groupData) {
      groupData = groupData
        .map((userData) => userData.map((fields, index) => ({ index: index + 1, ...fields })))
        .filter((list) => list.length >= count)
        .map((list) => list.slice(0, count));

      const flatData = groupData.flatMap((userData) => userData);
      const xValues = flatData.map((d) => d[xField]);
      const yValues = flatData.map((d) => d[yField]);
      const minXValue = Math.min(...xValues);
      const maxXValue = Math.max(...xValues);
      const maxYValue = Math.max(...yValues);
      const horizontalSpacing = (maxXValue - minXValue) * HORIZONTAL_SPACING || 1;
      const verticalSpacing = maxYValue * VERTICAL_SPACING;
      this.xScale = widthScale.domain([minXValue - horizontalSpacing, maxXValue + horizontalSpacing]);
      this.yScale = heightScale.domain([maxYValue + verticalSpacing, -verticalSpacing]);

      const sizeArray = groupData.length > 0 ? [...Array(count)] : [];
      const xSeries = sizeArray.map((_, index) => this.xScale(index + 1));
      const ySeries = sizeArray.map((_, index) => {
        const values = groupData.map((userData) => userData[index][yField]);
        return this.yScale(getAverage(values));
      });
      const result = polynomial(xSeries, ySeries, order);
      const equation = calculateY(result.equation);

      // Update no data message
      svg
        .select('.no-data')
        .transition()
        .duration(TRANSITION_DURATION / 2)
        .style('opacity', flatData.length === 0 ? 1 : 0);

      // Update axis
      chart
        .select('.x-axis')
        .transition()
        .duration(TRANSITION_DURATION)
        .call(axisBottom(this.xScale).tickSizeInner(-height));

      chart
        .select('.y-axis')
        .transition()
        .duration(TRANSITION_DURATION)
        .call(axisLeft(this.yScale).ticks(10).tickSizeInner(-width));

      // Update datapoints
      const dataPoints = chart.selectAll('.data-point').data(flatData);

      // Remove old datapoints
      dataPoints
        .exit()
        .transition()
        .duration(TRANSITION_DURATION / 2)
        .style('opacity', 0)
        .remove();

      // Add new datapoints
      dataPoints
        .enter()
        .append('circle')
        .attr('class', 'data-point')
        .style('opacity', 0)
        .style('stroke', (d) => d.color)
        .style('fill', (d) => d.color)
        .attr('cx', (d) => this.xScale(d[xField]))
        .attr('cy', (d) => this.yScale(d[yField]))
        .attr('r', DATA_POINT_RADIUS)
        .on('mouseover', (event, d) => {
          this.updateTooltip(event, d, yField);
        })
        .on('mouseout', () => {
          select('#tooltip').style('visibility', 'hidden');
        })
        .transition()
        .duration(TRANSITION_DURATION / 2)
        .style('opacity', 1);

      // Update existing datapoints
      dataPoints
        .on('mouseover', (d) => {
          this.updateTooltip(d, yField);
        })
        .on('mouseout', () => {
          select('#tooltip').style('visibility', 'hidden');
        })
        .transition()
        .duration(TRANSITION_DURATION)
        .attr('cx', (d) => this.xScale(d[xField]))
        .attr('cy', (d) => this.yScale(d[yField]))
        .style('fill', (d) => d.color)
        .style('stroke', (d) => d.color)
        .style('opacity', 1);

      // Update regression line
      const newLine = line()
        .curve(curveBasis)
        .x((d) => d)
        .y((d) => equation(d));
      const regressionLine = chart.selectAll('.regression-line').datum(xSeries);
      regressionLine
        .enter()
        .append('path')
        .attr('class', 'regression-line')
        .style('opacity', 0)
        .style('stroke', 'red')
        .attr('d', newLine)
        .transition()
        .duration(TRANSITION_DURATION / 2)
        .style('opacity', 1);
      regressionLine
        .transition()
        .duration(TRANSITION_DURATION)
        .attrTween('d', (d) => {
          const previous = regressionLine.attr('d');
          const current = newLine(d);
          return interpolatePath(previous, current);
        })
        .style('opacity', 1)
        .style('stroke', 'red');
      regressionLine
        .exit()
        .transition()
        .duration(TRANSITION_DURATION / 2)
        .style('opacity', 0)
        .remove();
    } else {
      console.log('Unknown props changed');
    }
  }

  componentWillUnmount() {
    if (!select('#tooltip').empty()) {
      select('#tooltip').remove();
    }
  }

  updateTooltip = (event, d, yField) => {
    select('#tooltip')
      .style('visibility', 'visible')
      .style('left', `${event.pageX + 20}px`)
      .style('top', `${event.pageY - 75}px`)
      .html(
        `<div>${this.props.xLabel}: ${d[this.props.xField]}</div>
        <div>${this.props.yLabel}: ${d[yField]}</div>`,
      );
  };

  render() {
    return (
      <svg
        ref={(svg) => {
          this.svgRef = svg;
        }}
        width={this.props.width}
        height={this.props.height}
      />
    );
  }
}

GroupRegressionChart.propTypes = {
  width: PropTypes.number.isRequired,
  height: PropTypes.number.isRequired,
  xLabel: PropTypes.string.isRequired,
  yLabel: PropTypes.string.isRequired,
  xField: PropTypes.string.isRequired,
  yField: PropTypes.string.isRequired,
  count: PropTypes.number.isRequired,
  order: PropTypes.number.isRequired,
  groupData: PropTypes.arrayOf(PropTypes.arrayOf(PropTypes.object)).isRequired,
};

export default GroupRegressionChart;
